class MyMailer < ApplicationMailer

  def send_email_contact(name, email, phone, message)
    @name = name
    @email = email
    @phone = phone
    @message = message

    mail(from: email, subject: 'Contact Us', to: "codelit@hotmail.com")
  end

end
